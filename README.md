# python_task_brainfork

Brainfork
=========
Brainfork is a multithreaded variant of Brainfuck, invented by Asger Ipsen in 2004. Brainfork retains Brainfuck's eight 
commands and adds a ninth, Y, for forking. When a Y is encountered, the current thread forks, with the current cell 
being zeroed in the parent thread, and the pointer being moved one to the right and that cell set to 1 in the child.
Because Brainfork is a proper superset of Brainfuck, all Brainfuck programs that do not contain the letter Y are also 
valid Brainfork programs with identical semantics. Because Brainfuck ignores all unknown instructions (such as Y), all 
Brainfork programs are also valid Brainfuck programs, although they may (of course) behave very differently.

How to Run
==========
You can run from console with code in another file.
You should write this:
brainfork>python brainfork file_name [--input input_data]

Also, you can use the realtime-input mode like:
brainfork>python brainfork file_name
>input_data_per_line

For example:
1. ...\brainfork>python brainfork.py ./example/example.bfo
2. ...\brainfork>python brainfork.py ./examples/hello_input.bf --input 72 69 76 76 79
2. ...\brainfork>python brainfork.py ./examples/hello_input.bf
>72 
>69 
>76 
>76 
>79

| File Name     | Content                                           |
| ------------- | --------------------------------------------------|
| brainfork.py  | Simple brainfork realization                      |
| test_all.py   | Tests for all brainfork functions                 |
| examples      | Here you can find some examples of bf or bfo code |

Links
=====
https://esolangs.org/wiki/Brainfork