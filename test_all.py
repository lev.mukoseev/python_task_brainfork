from brainfork import Brainfork, BrainforkErrors
import unittest


class TestFiltration(unittest.TestCase):
    def test_empty(self):
        self.assertEqual('', Brainfork('').filtered_code)

    def test_correct(self):
        self.assertEqual('><+-.,[]', Brainfork('><+-.,[]').filtered_code)

    def test_wrong(self):
        self.assertEqual('', Brainfork('kto ya? кто_9`~\\/\"').filtered_code)

    def test_mixed(self):
        self.assertEqual('><+-.,[]',
                         Brainfork('>vd<?+23-~.,[]\n').filtered_code)

    def test_wrong_brackets(self):
        self.assertEqual('[]', Brainfork('][][').filtered_code)


class TestBlocks(unittest.TestCase):
    def test_empty_input(self):
        self.assertEqual({}, Brainfork("").blocks)

    def test_no_blocks(self):
        self.assertEqual({}, Brainfork("dfvneorwwivncdslk").blocks)

    def test_simple_block(self):
        self.assertEqual({2: 3, 3: 2}, Brainfork(".,[],.").blocks)

    def test_blocks(self):
        self.assertEqual({2: 5, 5: 2, 3: 4, 4: 3, 8: 12, 12: 8},
                         Brainfork(",.[[]]cw[dsc]").blocks)


class TestErrors(unittest.TestCase):
    def test_wrong_symbol(self):
        self.assertEqual(BrainforkErrors.ERRORS[0],
                         Brainfork('>?+').errors.errors[1])

    def test_block_not_opened(self):
        self.assertEqual(BrainforkErrors.ERRORS[1],
                         Brainfork('][]').errors.errors[0])

    def test_block_not_closed(self):
        self.assertEqual(BrainforkErrors.ERRORS[2],
                         Brainfork('[][').errors.errors[2])

    def test_trying_get_negative_index(self):
        try:
            bf = Brainfork('<++<')
            bf.run()
        except Exception as e:
            self.assertTrue(type(e) is IndexError)

    def test_make_code_notes(self):
        self.assertEqual('A  A    A',
                         BrainforkErrors.make_code_notes(9, {0, 3, 8}, 'A'))

    def test_errors_pointer(self):
        code = ">>+H+"
        bf = Brainfork(code)
        bf.run()
        self.assertEqual(f'{code}\n   ^ \n   | ', bf.errors.get_errors())


class TestBrainfuck(unittest.TestCase):
    def test_with_input(self):
        user_input = list(map(ord, ['H', 'E', 'L', 'L', 'O']))
        code = ',.>,.>,.>,.>,.'
        bf = Brainfork(code, False, user_input)
        bf.run()
        output = bf.output
        self.assertEqual(output, 'HELLO')

    def test_print_message(self):
        bf = Brainfork("++++++++++[>+++++++>+++++++++" +
                       "+>+++>+<<<<-]>++.>+.+++++++.." +
                       "+++.>++.<<+++++++++++++++.>.+" +
                       "++.------.--------.>+.>")
        bf.run()
        self.assertEqual("Hello World!", bf.output)

    def test_empty_input(self):
        bf = Brainfork('')
        bf.run()
        self.assertEqual('', bf.output)


if __name__ == '__main__':
    unittest.main()
