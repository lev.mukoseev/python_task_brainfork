import random
import argparse
import  sys


class Memory:
    def __init__(self, memory=None, index=0, code_index=0, args_index=0):
        if memory is None:
            memory = {0: 0}
        self.memory = memory
        self.index = index
        self.code_index = code_index
        self.args_index = args_index


class Manager:
    def __init__(self, code_len: int, points_base=10, points_delta=5):
        self.code_len = code_len
        self.points_base = points_base
        self.points_delta = points_delta

    def manage(self, processes):
        process = None
        while processes:
            process = random.choice(tuple(processes))
            if process.code_index < self.code_len:
                break
            processes.remove(process)
            process = None

        if process is None:
            return None, 0
        points = random.randint(-self.points_delta, self.points_delta) \
                 + self.points_base
        points = min(points, self.code_len - process.code_index)
        return process, points


class IO:
    def __init__(self, entry_func, output_func):
        self.entry_func = entry_func
        self.output_func = output_func


class BrainforkErrors:
    def __init__(self, input_code):
        self.blocked = False
        self.errors = dict()
        self.input_code = input_code
        self.filtered_code = self.filtration(input_code)

    ERRORS = [
        "Wrong Symbol",
        "Invalid Bracket Sequence: Not Open",
        "Invalid Bracket Sequence: Not Closed",
        "Trying to Address a Negative Index"
    ]
    COMMANDS = {'>', '<', '+', '-', '.', ',', '[', ']', 'Y'}

    @staticmethod
    def make_code_notes(length, points, symbol):
        return ''.join([symbol if i in points else ' ' for i in range(length)])

    def get_errors(self):
        return "\n".join([
            self.input_code,
            self.make_code_notes(len(self.input_code), self.errors, '^'),
            self.make_code_notes(len(self.input_code), self.errors, '|')])

    def get_errors_detailed(self):
        return "\n".join([self.get_errors()]
                         + [f'On Index {i}: {self.errors[i]}'
                            for i in self.errors])

    def filtration(self, input_code):
        open_count = 0
        open_index = []
        for index, symbol in enumerate(input_code):
            if symbol not in self.COMMANDS:
                self.errors[index] = self.ERRORS[0]
            elif symbol == '[':
                open_index.append(index)
                open_count += 1
            elif symbol == ']' and open_count == 0:
                self.errors[index] = self.ERRORS[1]
            elif symbol == ']' and open_count > 0:
                open_count -= 1

        for i in range(max(open_count, 0)):
            self.errors[open_index[-i - 1]] = self.ERRORS[2]

        if self.errors:
            print(self.get_errors_detailed())
            self.blocked = True

        return ''.join([symbol for index, symbol in enumerate(input_code)
                        if index not in self.errors])


class Brainfork:
    COMMAND_PROCESSOR = {}

    def __init__(self, input_code: str, io=None, input_values=None):
        self.realtime = input_values is None
        if self.realtime:
            input_values = []
        if io is None:
            io = IO(input, lambda x: print(x))
        self.io = io
        self.processes = set()
        self.COMMAND_PROCESSOR = {
            '>': self.index_up,
            '<': self.index_down,
            '+': self.value_up,
            '-': self.value_down,
            '.': self.print_value,
            ',': self.input_value,
            'Y': self.fork,
            '[': self.loop_start,
            ']': self.loop_end
        }
        self.input_code = input_code
        self.input_values = input_values
        self.output = ''
        self.errors = BrainforkErrors(input_code)
        self.filtered_code = self.errors.filtered_code
        self.blocks = self.get_blocks()

    def get_blocks(self):
        blocks = {}
        opened = []
        for index, symbol in enumerate(self.input_code):
            if symbol == '[':
                opened.append(index)
            elif symbol == ']' and opened:
                blocks[index] = opened[-1]
                blocks[opened.pop()] = index
        return blocks

    def index_up(self, memory):
        memory.index += 1
        memory.memory.setdefault(memory.index, 0)

    def index_down(self, memory):
        if memory.index > 0:
            memory.index -= 1
        else:
            raise IndexError("Memory Index Out Of Bounds On Index "
                             f"{memory.code_index}")

    def value_up(self, memory):
        memory.memory[memory.index] += 1

    def value_down(self, memory):
        memory.memory[memory.index] -= 1

    def print_value(self, memory):
        symbol = chr(memory.memory[memory.index])
        self.output += symbol
        if self.realtime:
            self.io.output_func(symbol)

    def input_value(self, memory):
        if memory.args_index >= len(self.input_values):
            memory.memory[memory.index] = int(self.io.entry_func())
        else:
            memory.memory[memory.index] = self.input_values[memory.args_index]
        memory.args_index += 1

    def loop_start(self, memory):
        if not memory.memory[memory.index]:
            memory.code_index = self.blocks[memory.code_index]

    def loop_end(self, memory):
        if memory.memory[memory.index]:
            memory.code_index = self.blocks[memory.code_index]

    def fork(self, memory):
        new_memory = Memory(memory.memory.copy(),
                            memory.index,
                            memory.code_index + 1,
                            memory.args_index)
        new_memory.memory[new_memory.index] = 1
        memory.memory[memory.index] = 0
        self.processes.add(new_memory)

    def run(self):
        code_len = len(self.filtered_code)
        memory = Memory()
        manager = Manager(code_len)
        self.processes.add(memory)
        while self.processes and not self.errors.blocked:
            current_memory, points = manager.manage(self.processes)
            for i in range(points):
                sym = self.filtered_code[memory.code_index]
                self.COMMAND_PROCESSOR[sym](current_memory)
                memory.code_index += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_name',
                        type=str,
                        help="Name of file with bf code")
    parser.add_argument('--input',
                        nargs='+',
                        default=[],
                        type=int,
                        help="Array of input values")
    parser.add_argument("--read_int", help="Read input in int")
    parser.add_argument("--out_int", help="Print result in int")
    args = parser.parse_args()
    with open(args.file_name, 'r') as f:
        code = f.read()
    realtime = args.input is None
    io = IO(input, print)
    brainfork = Brainfork(code, io, args.input)
    brainfork.run()
    print(brainfork.output)
